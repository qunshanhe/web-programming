# WebProgramming

#### 介绍
本仓库为基于Web的网络编程课程作业及实验代码，NetBeans工程。

#### 源码文件介绍
|文件     |功能     |
| ------- | ------ |
|StudenRegister/StudentRegister.html| 实验一-学生注册HTML网页设计 |
|ClassWork/FactorialSum.java| 实验二-计算1到n的阶乘和 |
|ClassWork/WordReplace.java| 实验三-替换文本中的单词 |
|ClassWork/GraphMaker.java| 实验四-实现正、倒直角三角形以及菱形的输出 |
|ClassWork/ArrayExercise.java| 实验五-数组练习实现分数段人数统计 |
|StudentClass/Student.java| 实验六-面向对象程序设计训练-学生类的定义 |
|StudentPro| 实验七-类的继承、多态 |
|PackageExercise| 实验八-包的训练 |
---