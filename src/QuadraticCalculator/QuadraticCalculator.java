/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QuadraticCalculator;

import java.io.*;
import java.util.logging.*;

/**
 *
 * @author Mountain
 */
public class QuadraticCalculator {
    // a*x^2+b*x+c=0;
    double a;
    double b;
    double c;
    
    public void setCoeff(){
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("请输入一元二次方程（a*x^2+b*x+c=0）的参数：");
            System.out.print("a:");
            String s=in.readLine();
            a=Double.parseDouble(s);
            System.out.print("b:");
            s=in.readLine();
            b=Double.parseDouble(s);
            System.out.print("c:");
            s=in.readLine();
            c=Double.parseDouble(s);
        } catch (IOException ex) {
            Logger.getLogger(QuadraticCalculator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void showEquation(){
        System.out.println("设定的一元二次方程为："+a+"*x^2 + "+b+"*x +"+c+" = 0");
    }
    
    public void solveEquation(){
        double d = b*b-4*a*c;
        if(Math.abs(d)<1e-8){
            double x12=-b/(2*a);
            System.out.println("该一元二次方程有两个相同的实根：x1,x2="+x12);
        }
        else if(d>0){
            double dc = Math.sqrt(d);
            double x1=(-b+dc)/(2*a);
            double x2=(-b-dc)/(2*a);
            System.out.println("该一元二次方程有两个不相同的实根："+"x1="+x1+"; x2="+x2);
        }
        else{
            double dc = Math.sqrt(Math.abs(d));
            double r=(-b)/(2*a);
            double i=(dc)/(2*a);
            System.out.println("该一元二次方程有两个不相同的复根："+"x1="+r+"+i*"+i+"; x2="+r+"-i*"+i);
        }
    }
    
    public void run(){
        do{
            try {
                setCoeff();
                showEquation();
                solveEquation();
                BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Do you want to continue?(Y/N)");
                int key=in.read();
                if(key=='N'||key=='n')
                    break;
            } catch (IOException ex) {
                Logger.getLogger(QuadraticCalculator.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }while(true);
    }
    
    public static void main(String[] args) {
        // TODO code application logic here
        QuadraticCalculator calculator = new QuadraticCalculator();
        calculator.run();
    }
}
