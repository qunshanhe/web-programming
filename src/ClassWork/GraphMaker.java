/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClassWork;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mountain
 */
public class GraphMaker {
    public static void drawRhombic(int n){
        n = n%2==0 ? n+1: n;
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                if(Math.abs(j-n/2)<=(n/2-Math.abs(n/2-i))){
                    System.out.print("*");
                }
                else{
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
    
    public static void drawTriangle(int n){
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                if(j<=i){
                    System.out.print("*");
                }
                else{
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
    
    public static void drawInvertTriangle(int n){
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                if(j>=i){
                    System.out.print("*");
                }
                else{
                    System.out.print(" ");
                }
            }
            System.out.println();
        } 
    }
    
    public static void main(String args[]){
        int n = 3;
        int type = 1;
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String s;
        do{
            try {
                System.out.print("请输入需要绘制图形的类型（1：菱形；2：三角形；3：倒三角形）：");
                s = in.readLine();
                type = Integer.parseInt(s);
                System.out.print("请输入需要绘制图形的阶数：");
                s = in.readLine();
                n = Integer.parseInt(s);             
            } catch (IOException ex) {
                Logger.getLogger(GraphMaker.class.getName()).log(Level.SEVERE, null, ex);
            }
        switch(type){
            case 1: 
                drawRhombic(n);         break;
            case 2:
                drawTriangle(n);        break;
            case 3:
                drawInvertTriangle(n);  break;
            default:
                System.out.println("图形类型输入有误！");
        }
        System.out.print("Do you want to continue? (Y:yes; N:no)");
            try {
                s = in.readLine();
                if(s.equalsIgnoreCase("N"))
                    break;
            } catch (IOException ex) {
                Logger.getLogger(GraphMaker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }while(true);

    }
    
}
