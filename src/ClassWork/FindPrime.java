/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClassWork;

/**
 *
 * @author Mountain
 */
public class FindPrime {
    public static boolean isPrime(int num){
        for(int i=2;i<Math.sqrt(num)+1;i++){
            if(num%i==0)
                return false;
        }
        return true;
    }
    
    public static void main(String argv[]){
        System.out.println("100以内的素数有：");
        for(int  i=1;i<=100;i++){
            if(isPrime(i)){
                System.out.print(i+" ");
            }
        }
    }
}
