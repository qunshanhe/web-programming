/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClassWork;

import java.io.*;
import java.util.logging.*;

/**
 *
 * @author Mountain
 */
public class Goldbach {
 
    public static boolean isPrime(long num){
        for(long i=2;i<Math.sqrt(num)+1;i++){
            if(num%i==0)
                return false;
        }
        return true;
    }
    
    public static long Goldbach(long EvenNum){
        long prime1 = 0;
        long prime2 = 0;
        
        for(long i=3; i<EvenNum; i++){
            prime1 = i;
            prime2 = EvenNum - prime1;
            if(isPrime(prime1) && isPrime(prime2)){
                break;
            }
        }
        return prime1;
    }
    
    
    public static void main(String args[]){  
        while(true){
            try {
                System.out.print("Please input n: ");
                BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
                String s = in.readLine();
                long  n = Long.parseLong(s);
                if(n%2!=0){
                    System.out.println("Input Error! This is not an Even Number!");
                }
                else{
                    long prime1 = Goldbach(n);
                    System.out.println("EnevNumber: "+ n + " can be splited into two prime Numbers: ");
                    System.out.println(prime1+" + "+ (n-prime1)+ " = " + n);
                }
                System.out.println("Do you want to continue?(Y/N)");
                int key=in.read();
                if(key=='N'||key=='n')
                    break;
                System.out.println("-----------------------------");
            } catch (IOException ex) {
                Logger.getLogger(Goldbach.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    
}
