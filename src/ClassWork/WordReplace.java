/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClassWork;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mountain
 */
public class WordReplace {
    String srcText;
    String dstText;
    String srcWord;
    String dstWord;
    int beginIndex;
    int endIndex;
    
    public void getSrcText(){
        try {
            System.out.println("请输入源文档：");
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            srcText = in.readLine();
        } catch (IOException ex) {
            Logger.getLogger(WordReplace.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void setSrcWord(){
        try {
            System.out.print("请输入需要被替换的单词：");
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            srcWord = in.readLine();
        } catch (IOException ex) {
            Logger.getLogger(WordReplace.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void setDstWord(){
        try {
            System.out.print("请输入替换之后的单词：");
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            dstWord = in.readLine();
        } catch (IOException ex) {
            Logger.getLogger(WordReplace.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String getReplacedText(){
        boolean isFound = false;
        beginIndex = 0;
        endIndex = 0; // 当前单词后面首个空格所在的索引号
        dstText = srcText+" ";
        String subText = srcText;
        while(endIndex != dstText.length()-1){
            endIndex = subText.indexOf(" ") + beginIndex;
            String currentWord = dstText.substring(beginIndex,endIndex);
            if(srcWord.equalsIgnoreCase(currentWord)){
                String formerText = dstText.substring(0, beginIndex);
                String latterText = dstText.substring(endIndex);
                dstText = formerText + dstWord + latterText;
                endIndex = beginIndex + dstWord.length();
                isFound = true; 
            }
            subText = dstText.substring(endIndex+1);
            beginIndex = endIndex+1;
        }

        if(isFound){
            System.out.println("替换之后的句子为：\n"+dstText);
        }
        else{
            System.out.println("Word: "+srcWord+" is not found!");
        }
        return dstText;
    }
    
    public static void main(String args[]){

        WordReplace wordReplacer = new WordReplace();
        wordReplacer.getSrcText();
        wordReplacer.setSrcWord();
        wordReplacer.setDstWord();
        wordReplacer.getReplacedText();
        
/*               "Sometimes life can hit you in the head with a brick. Don't lose faith. ";
               + "I'm convinced that the only thing that kept me going was I love what I did. "
                + "You've got to find what you love. And that is true for your work as it is for your lovers. "
                + "Your work is going to take a large part of your life, "
                + "and the only way to be truly satisfied is to do what you believe is great work. "
                + "And the only way to do great work is to love what you do. "
                + "If you haven't found it yet, keeping looking. Don't settle. "
                + "As with all matters of the heart, you'll know when you find it. "
                + "And like any great relationship, it just gets better and better as the year rool on. "
                + "So keep looking. Don't settle.";
*/

        
    }
    
    public static void replaceWord(String srcText, String dstText, String dstWord, int beginIndex, int endIndex){
        String formerText = srcText.substring(0, beginIndex);
        String latterText = srcText.substring(endIndex);
        dstText = formerText + dstWord + latterText;
        endIndex = beginIndex + dstText.length();
    }
    
}
