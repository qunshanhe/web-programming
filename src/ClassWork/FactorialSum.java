/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClassWork;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mountain
 */
public class FactorialSum {
    public static void main(String srgv[]){
        try {
            System.out.print("Please input n: ");
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            String s = in.readLine();
            int n=Integer.parseInt(s);
            int factorialSum = 0;
            for(int i=1;i<=n;i++){
                int factorial = 1;
                for(int j=1;j<=i;j++){
                    factorial *= j ;
                }
                factorialSum += factorial;
            }
            System.out.println("从1到"+n+"的阶乘的和为："+factorialSum);
        } catch (IOException ex) {
            Logger.getLogger(FactorialSum.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
