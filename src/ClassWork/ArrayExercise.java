/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClassWork;

/**
 *
 * @author Mountain
 */
public class ArrayExercise {
    public static void sortInc(int numbers[]){
        int n = numbers.length;
        int swap;
        for(int i=0;i<n;i++){
            for(int j=0;j<n-i-1;j++){
                if(numbers[j]>numbers[j+1]){
                    swap = numbers[j];
                    numbers[j] = numbers[j+1];
                    numbers[j+1]=swap;
                }
            }
        }
    }
    
    public static void showArray(int numbers[]){
        int n = numbers.length;
        for(int i=0;i<n;i++){
            System.out.print(numbers[i]);
            System.out.print(" ");
        }
    }
    
    public static void showScoreDistribution(int scores[]){
        int A = 0;
        int B = 0;
        int C = 0;
        int D = 0;
        for(int i=0;i<scores.length;i++){
            if(scores[i]>=90)
                A++;
            else if(scores[i]>=80)
                B++;
            else if(scores[i]>=60)
                C++;
            else
                D++;
        }
        System.out.println("成绩为优秀A(90~100)的学生数量："+A);
        System.out.println("成绩为良好B(80~89 )的学生数量："+B);
        System.out.println("成绩为优秀C(60~79 )的学生数量："+C);
        System.out.println("成绩为优秀D( 0~59 )的学生数量："+D);
    }
    
    public static void main(String args[]){
        int numbers[] = {7,2,3,8,4,6,1,9,4,6};
        System.out.print("Before sort increasingly: ");
        showArray(numbers);
        System.out.println();
        sortInc(numbers);
        System.out.print("After  sort increasingly: ");
        showArray(numbers);
        System.out.println();
        
        int mathScores[] = {86,90,95,36,89,76,44,67,97,86,56,77,79,99,100,69,85,76,63,87};
        showScoreDistribution(mathScores);
    }
}
