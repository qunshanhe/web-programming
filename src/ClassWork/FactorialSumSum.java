/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClassWork;

import java.io.*;
import java.util.logging.*;

/**
 *
 * @author Mountain
 */
public class FactorialSumSum {
    public static long getFactorialSum(long n){
        long factorialSum = 0;
        for(long i=1;i<=n;i++){
            long factorial = 1;
            for(long j=1;j<=i;j++){
                factorial *= j ;
            }
            factorialSum += factorial;
        }
        return factorialSum;
    }
    
    public static void main(String srgv[]){
        try {
            System.out.print("Please input n: ");
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            String s = in.readLine();
            long n=Long.parseLong(s);
            long factorialSumSum = 0;
            long factorialSum = 0;
            for(long i=1;i<=n;i++){
                factorialSum = getFactorialSum(i);
                factorialSumSum += factorialSum;
            }
            System.out.println("从1到"+n+"的阶乘和的和为："+factorialSumSum);
        } catch (IOException ex) {
            Logger.getLogger(FactorialSum.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
