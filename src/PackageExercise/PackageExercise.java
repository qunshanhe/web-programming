/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import StudentClass.Student;

/**
 *
 * @author Mountain
 */
public class PackageExercise {
    
    public static int getMathMax(Student students[]){
        int max = 0;
        for(int i=0;i<students.length;i++){
            if(students[i].getMathScore()>max)
                max = students[i].getMathScore();
        }
        return max;
    }
    
    public static int getMathMin(Student students[]){
        int min = Integer.MAX_VALUE;
        for(int i=0;i<students.length;i++){
            if(students[i].getMathScore()<min)
                min = students[i].getMathScore();
        }
        return min;
    }
    
    public static int getEnglishMax(Student students[]){
        int max = 0;
        for(int i=0;i<students.length;i++){
            if(students[i].getEnglishScore()>max)
                max = students[i].getEnglishScore();
        }
        return max;
    }
    
    public static int getEnglishMin(Student students[]){
        int min = Integer.MAX_VALUE;
        for(int i=0;i<students.length;i++){
            if(students[i].getEnglishScore()<min)
                min = students[i].getEnglishScore();
        }
        return min;
    }
    
    public static int getComputerMax(Student students[]){
        int max = 0;
        for(int i=0;i<students.length;i++){
            if(students[i].getComputerScore()>max)
                max = students[i].getComputerScore();
        }
        return max;
    }
    
    public static int getComputerMin(Student students[]){
        int min = Integer.MAX_VALUE;
        for(int i=0;i<students.length;i++){
            if(students[i].getComputerScore()<min)
                min = students[i].getComputerScore();
        }
        return min;
    }
    public static void main(String args[]){
        Student[] students = new Student[10];
        students[0] = new Student("A","20180801",99,96,94);
        students[1] = new Student("B","20180802",86,86,94);
        students[2] = new Student("C","20180803",82,96,94);
        students[3] = new Student("D","20180804",68,74,99);
        students[4] = new Student("E","20180805",76,98,94);
        students[5] = new Student("F","20180806",88,96,78);
        students[6] = new Student("G","20180807",86,86,64);
        students[7] = new Student("H","20180808",94,76,64);
        students[8] = new Student("I","20180809",79,86,84);
        students[9] = new Student("J","20180810",68,99,97);
        
        System.out.println("The max mathScore in these students is: "+getMathMax(students));
        System.out.println("The max englishScore in these students is: "+getEnglishMax(students));
        System.out.println("The max computerScore in these students is: "+getComputerMax(students));
        System.out.println("The min mathScore in these students is: "+getMathMin(students));
        System.out.println("The min englishScore in these students is: "+getEnglishMin(students));
        System.out.println("The min computerScore in these students is: "+getComputerMin(students));
    }
}
