/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Student;

/**
 *
 * @author Qunshan He 20180822
 */
public class Student {
    Date payDate;
    String name;
    int age;
    
    public Student() {
        payDate = new Date(2018,8,1);
        name = new String();
        age = 18;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public void setAge(int age){
        this.age = age;
    }
    
    public void setPayDate(int year,int month, int day){
        payDate.setDate(year, month, day);
    }
    
    public void showName(){
        System.out.println("姓名:"+name);
    }

    public void showAge(){
        System.out.println("年龄:"+age);
    }
   
    public void showPayDate(){
        System.out.print("缴费日期:");
        payDate.showDate();
    }
    
    public static void main(String args[]){
        Student student = new Student();
        student.setName("小明");
        student.setAge(20);
        student.setPayDate(2021,4,14);
        student.showName();
        student.showAge();
        student.showPayDate();
    }
    
}
