/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Student;

/**
 *
 * @author Qunshan He 20180822
 */
public class Date {
    int year;
    int month;
    int day;
    
    public Date(int year, int month, int day){
        this.year = year;
        this.month = month;
        this.day = day;
    }
    
    public void setDate(int year, int month, int day){
        this.year = year;
        this.month = month;
        this.day = day;
    }
    
    public void showDate(){
        System.out.print(year+"Y/"+month+"M/"+day+"D/");
    }
    
    public boolean equal(Date date2){
        if(this.year==date2.year&&this.month==date2.month&&this.day==date2.day){
            return true;
        }
        return false;
    }
}
