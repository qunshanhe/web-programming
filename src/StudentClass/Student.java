/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package StudentClass;

/**
 *
 * @author Mountain
 */
public class Student {
    // 静态成员
    static String major;
    // 实例成员
    String name;
    String ID;
    int mathScore;
    int englishScore;
    int computerScore;
    
    public Student(){
        name = "Default";
        ID = "20180101";
        mathScore = 60;
        englishScore = 60;
        computerScore = 60;
    }
    
    public Student(String name, String ID, int mathScore, int englishScore, int computerScore){
        this.name = name;
        this.ID = ID;
        this.mathScore = mathScore;
        this.englishScore = englishScore;
        this.computerScore = computerScore;
    }
    
    //静态方法
    public static void setMajor(String major){
        Student.major = major; 
    }
    
    // 实例方法
    public void setName(String name){
        this.name = name;
    }
    
    public void setID(String ID){
        this.ID = ID;
    }
    
    public void setMathScore(int mathScore){
        this.mathScore = mathScore;
    }
    
    public void setEnglishScore(int englishScore){
        this.englishScore = englishScore;
    }
    
    public void setComputerScore(int computerScore){
        this.computerScore = computerScore;
    }

    //静态方法
    public static void showMajor(){
        System.out.println("Major: \t\t"+major);
    }
    
    public void showName(){
        System.out.println("Name: \t\t"+name);
    }
    
    public void showID(){
        System.out.println("ID: \t\t"+ID);
    }
    
    public void showMathScore(){
        System.out.println("MathScore: \t"+mathScore);
    }
    
    public void showEnglishScore(){
        System.out.println("EnglishScore: \t"+englishScore);
    }
    
    public void showComputerScore(){
        System.out.println("ComputerScore: \t"+computerScore);
    }
    
    public int getMathScore(){
        return mathScore;
    }
    
    public int getEnglishScore(){
        return englishScore;
    }
    
    public int getComputerScore(){
        return computerScore;
    }
    
    public static void main(String args[]){
        Student CEStudent = new Student();
        CEStudent.setName("Qunshan He");
        CEStudent.setID("20180822");
        CEStudent.setMathScore(98);
        CEStudent.setEnglishScore(97);
        CEStudent.setComputerScore(93);
        Student.setMajor("Communication Engineering");
        Student.showMajor();
        CEStudent.showName();
        CEStudent.showID();
        CEStudent.showMathScore();
        CEStudent.showEnglishScore();
        CEStudent.showComputerScore();
    }
    
}
