/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BigLottery;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mountain
 */
public class BigLottery {
    int fp,sp,tp;
    int buy;
    
    public void buyLottery(){
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("请输入您要购买的彩票号（1-20）：");
            String s=in.readLine();
            System.out.println("您购买的彩票号码为："+s+"。请等待开奖，祝君好运。");
            buy=Integer.parseInt(s);
        } catch (IOException ex) {
            Logger.getLogger(BigLottery.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void drawLottery(){
        try {
            System.out.println("开奖中...");
            Thread.sleep(2000);
            fp=(int)(Math.random()*20)+1;
            do{
                sp=(int)(Math.random()*20)+1;
            }while(sp==fp);
            do{
                tp=(int)(Math.random()*20)+1;
            }while(tp==sp||tp==fp);
            System.out.println("开奖完毕，本次彩票奖号公布如下：");
            System.out.println("一等奖："+fp+"；二等奖："+sp+"；三等奖："+tp);
        } catch (InterruptedException ex) {
            Logger.getLogger(BigLottery.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void checkLottery(){
        if(buy==fp){
            System.out.println("恭喜您！中得一等奖！");
        }
        else if(buy==sp){
            System.out.println("恭喜您！中得二等奖！");
        }
        else if(buy==tp){
            System.out.println("恭喜您！中得三等奖！");
        }
        else{
            System.out.println("本彩票无奖，祝君下次好运！");
        }
    }
    
    public void run(){
        buyLottery();
        drawLottery();
        checkLottery();
    }
    
    public static void main(String[] args) {
        // TODO code application logic here
        BigLottery bigLottery = new BigLottery();
        while(true){
            try {
                bigLottery.run();
                BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Do you want to continue?(Y/N)");
                int key=in.read();
                if(key=='N'||key=='n')
                    break;
                System.out.println("-----------------------------");
            } catch (IOException ex) {
                Logger.getLogger(BigLottery.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
