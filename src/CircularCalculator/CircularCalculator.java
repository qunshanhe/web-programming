/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CircularCalculator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mountain
 */
public class CircularCalculator {
    
    public static void main(String[] args) {
        try {
            // TODO code application logic here
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Please input the value of θ(degree): ");
            String s = in.readLine();
            Double value=Double.parseDouble(s);
            System.out.println("sin(θ)="+Math.sin(value/180*Math.PI)+"; cos(θ)="+Math.cos(value/180*Math.PI));
        } catch (IOException ex) {
            Logger.getLogger(CircularCalculator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
