/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package StudentPro;

/**
 *
 * @author Mountain
 */
public class Student {
    private String name;
    private int age;
    private double gpa;
    
    Student(){
        name = "Amelie";
        age = 18;
        gpa = 3.96;
    }
    
    Student(String name, int age, double gpa){
        this.name = name;
        this.age = age;
        this.gpa = gpa;
    }
    
    public void showName(){
        System.out.println("Name: \t"+ name);
    }
    
    public void showAge(){
        System.out.println("Age: \t"+ age);
    }
    
    public void showGPA(){
        System.out.println("GPA: \t"+ gpa);
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public void setAge(int age){
        this.age = age;
    }   
    
    public void setGPA(double gpa){
        this.gpa = gpa;
    }
    
}
