/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package StudentPro;

/**
 *
 * @author Mountain
 */
public class Main {
    public static void main(String args[]){
        Student std_a = new Student();
        System.out.println("------------------------");
        System.out.println("Infomation of Student a:");
        std_a.showName();
        std_a.showAge();
        std_a.showGPA();
        
        System.out.println("------------------------");
        System.out.println("Infomation of Student b:");
        Student std_b = new Student("Amma", 18, 3.86);
        std_b.showName();
        std_b.showAge();
        std_b.showGPA();
        System.out.println("One year later...");
        std_b.setAge(19);
        std_b.setGPA(3.91);
        std_b.showName();
        std_b.showAge();
        std_b.showGPA();
        
        System.out.println("------------------------");
        System.out.println("Infomation of Student C:");
        GraduateStudent std_c = new GraduateStudent();
        std_c.showName();
        std_c.showAge();
        std_c.showGPA();
        std_c.showDepartment();
        std_c.showMajor();;
    }
}
