/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package StudentPro;

/**
 *
 * @author Mountain
 */
public class GraduateStudent extends Student {
    String department;
    String major;
    
    public GraduateStudent(){
        department = "Psychology";
        major = "Advanced Psychology";
    }
    
    public GraduateStudent(String name, int age, double gpa, String department, String major) {
        super(name, age, gpa);
        this.department = department;
        this.major = major;
    }
    
    public void setDepartment(String department){
        this.department = department;
    }
    
    public void setMajor(String major){
        this.major = major;
    }
    
    public void showDepartment(){
        System.out.println("Department: \t"+ department);
    }
    
    public void showMajor(){
        System.out.println("Major: \t"+ major);
    }
}
