/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MatrixTransposer;

import java.io.*;

/**
 *
 * @author Qunshan He 20180822
 */
public class MatrixTransposer {
    int matrix[][];
    int rows;
    int cols;
    
    public MatrixTransposer(){
        rows = 10;
        cols = 10;
        matrix = new int[rows][cols];
    }
    
    void setMatrix(){
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("请输入行列式的行数：");
            String s = in.readLine();
            rows = Integer.parseInt(s);
            System.out.print("请输入行列式的列数：");
            s = in.readLine();
            cols = Integer.parseInt(s);
            matrix = new int[rows][cols];
            
            for(int i=0;i<rows;i++)
                for(int j=0;j<cols;j++){
                    System.out.print("请输入矩阵的第"+ (i+1) +"行"+ (j+1) +"列：");
                    s = in.readLine();
                    matrix[i][j]=Integer.parseInt(s);
                }
        } catch (IOException ex) { }
    }
    
    void showMatrix(){
        for(int i=0;i<rows;i++){
            for(int j=0;j<cols;j++){
                System.out.print(matrix[i][j]+"\t");
            }
            System.out.println();
        }
    }
    
    public void transposeMatrix(){
        int tempMatrix[][] = new int[cols][rows];
        for(int i=0;i<rows;i++)
            for(int j=0;j<cols;j++)
                tempMatrix[j][i]=matrix[i][j];
        int temp = rows;
        rows = cols;
        cols = temp;
        matrix = tempMatrix;
    }
    
    public static void main(String args[]){
        MatrixTransposer matrixTransposer = new MatrixTransposer();
        matrixTransposer.setMatrix();
        System.out.println("初始矩阵：");
        matrixTransposer.showMatrix();
        matrixTransposer.transposeMatrix();
        System.out.println("转置矩阵：");
        matrixTransposer.showMatrix();
    }
    
}
